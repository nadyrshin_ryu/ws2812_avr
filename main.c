//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-����� 
// "����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#include <ioavr.h>
#include <inavr.h>
#include "delay\delay.h"
#include "ws2812\ws2812.h"
#include "ws2812\led_examples.h"
#include "main.h"
#include <stdio.h>


void main( void )
{
  uint8_t EffectNum = 0;
  
  ws2812_init();
  
  while (1)
  {
    uint16_t Period;
    uint16_t TickNum = led_effect_start(EffectNum, &Period);
    while (TickNum--)
    {    
      led_effect_tick(EffectNum);
      ws2812_update();

      delay_ms(Period);
    }
    
    if (++EffectNum == LIGHT_EFFECTS_NUM)
      EffectNum = 0;
  }
}
